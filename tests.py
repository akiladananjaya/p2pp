from p2p import create_app
import unittest


class TestP2PP(unittest.TestCase):
    def setUp(self):
        self.app = create_app().test_client()

    def test_index_view(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_login_view(self):
        response = self.app.get('/login')
        self.assertEqual(response.status_code, 200)

    def test_register_view(self):
        response = self.app.get('/register')
        self.assertEqual(response.status_code, 200)

    def test_correct_login(self):
        respose = self.app.post('/login', data={"user-email": "vorivi2236@romails.net",
                                                "user-password": "vorivi2236@romails.net"}, follow_redirects=True)
        self.assertIn(b"Summary", respose.data)

    def test_incorrect_login(self):
        respose = self.app.post('/login', data={"user-email": "vorivi2236@romails.net",
                                                "user-password": "vori.net"}, follow_redirects=True)
        self.assertNotIn(b"Summary", respose.data)
