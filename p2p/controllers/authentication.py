from flask import Blueprint, render_template, redirect, flash, request, escape, url_for
from p2p.models.user import User
from p2p.models.account import Account
from p2p.models.transaction import Transaction
from werkzeug.security import generate_password_hash, check_password_hash
from p2p import db
from flask_login import login_user, logout_user, login_required
import uuid
import os

userAuth = Blueprint('auth', __name__)


@userAuth.route('/login')
def login_view():
    return render_template("auth/login.html")


@userAuth.route('/register')
def register_view():
    return render_template("auth/register.html")


@userAuth.route('/register', methods=["POST"])
def register():
    email = escape(request.form['user-email'])
    name = escape(request.form['user-name'])
    password = escape(request.form['user-password'])
    # todo: sanitize input data

    user = User.query.filter_by(email=email).first()
    if user:
        flash("You have already registered. Please Login", "error")
        return redirect(url_for("auth.login_view"))
    new_user = User(email=email, name=name,
                    password=generate_password_hash(password, method='sha256'))
    db.session.add(new_user)
    db.session.commit()

    # Open a default account with default balance for new users
    acc_number = str(uuid.uuid4())
    acc_balance = float(os.getenv("INITIAL_ACCOUNT_BALANCE"))
    new_account = Account(no=acc_number, balance=acc_balance, owner=new_user)
    db.session.add(new_account)
    db.session.commit()

    # check transaction logs to find received credits to registered user
    # and credit it to new account
    received_transactions = Transaction.query.filter_by(
        received_to=email).all()
    # Todo: get sum by sqlalchemy functions
    total_credited = sum(
        [transaction.amount for transaction in received_transactions])
    new_account.balance += total_credited
    db.session.commit()

    flash("Successfully registered! Please Login", "success")
    return redirect(url_for("auth.login_view"))


@userAuth.route('/login', methods=["POST"])
def login():
    email = escape(request.form['user-email'])
    password = escape(request.form['user-password'])

    user = User.query.filter_by(email=email).first()
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.', "error")
        return redirect(url_for("auth.login_view"))

    login_user(user)
    return redirect(url_for("dash.home_view"))


@userAuth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for("auth.login_view"))
