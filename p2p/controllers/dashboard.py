from flask import Blueprint, render_template, redirect, request, flash, escape, url_for
from flask_login import current_user, login_required
from p2p.models.user import User
from p2p.models.transaction import Transaction
from p2p import db
import uuid
from sqlalchemy import or_
from p2p.helpers.emails import sendCreditedEmail
import os

userDashboard = Blueprint('dash', __name__)


@userDashboard.route('/')
def index():
    return redirect(url_for("dash.home_view"))


@userDashboard.route('/home')
@login_required
def home_view():
    user_transactions = Transaction.query.order_by(Transaction.date_time.desc()).filter(or_(
        Transaction.sent_from == current_user.email, Transaction.received_to == current_user.email)).all()
    return render_template("home/home.html", currentUser=current_user, transactions=user_transactions)


@userDashboard.route('/transferMoney', methods=["POST"])
@login_required
def transterMoney():
    amount = escape(request.form["transfer-amount"])
    receiver_email = escape(request.form["receiver-email"])
    transferMessage = escape(request.form["transfer-message"])
    # TODO : sanitize above data

    # debit money in sender
    send_user = User.query.filter_by(email=current_user.email).first()
    # Account at least need to maintain Rs.1000
    if ((send_user.accounts[0].balance - float(amount)) < float(os.getenv("MINIMUM_ACCOUNT_BALANCE"))):
        flash(
            "Your last transaction cannot be completed due to insufficient balance", "error")
        return redirect(url_for("dash.home_view"))
    # debit amount can't be negative
    if (float(amount) <= 0):
        flash("Invalid transfer amount", "error")
        return redirect(url_for("dash.home_view"))
    # Money can't transfer to the same account
    if (current_user.email == receiver_email):
        flash("Your last transaction cannot be completed. Please check your information and try again!", "error")
        return redirect(url_for("dash.home_view"))

    send_user.accounts[0].balance -= float(amount)
    db.session.commit()

    # credit money to receiver if receiver is registered. otherwise skip
    receive_user = User.query.filter_by(email=receiver_email).first()
    if (receive_user):
        receive_user.accounts[0].balance += float(amount)
        db.session.commit()

    # create a new transaction log
    unique_id = str(uuid.uuid4())
    new_transaction = Transaction(uid=unique_id, sent_from=current_user.email,
                                  received_to=receiver_email, amount=amount, message=transferMessage)
    db.session.add(new_transaction)
    db.session.commit()

    sendCreditedEmail(receiver_email, current_user.email, str(amount))

    flash("Your last transaction completed successfully!", "success")

    return redirect(url_for("dash.home_view"))
