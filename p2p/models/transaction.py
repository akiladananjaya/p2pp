from p2p import db
import datetime


class Transaction(db.Model):
    uid = db.Column(db.String(36), primary_key=True)
    sent_from = db.Column(db.String(120))
    received_to = db.Column(db.String(120))
    amount = db.Column(db.Float())
    date_time = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
    message = db.Column(db.String(1024), default="")
