from p2p import db
from flask_login import UserMixin

class User(UserMixin, db.Model):
    email = db.Column(db.String(120), primary_key=True)
    password = db.Column(db.String(240))
    name = db.Column(db.String(240))
    accounts = db.relationship("Account", backref="owner")

    def get_id(self):
        return (self.email)