from p2p import db


class Account(db.Model):
    no = db.Column(db.String(36), primary_key=True)
    balance = db.Column(db.Float(100), nullable=False)
    owner_email = db.Column(db.String(120), db.ForeignKey('user.email'))
