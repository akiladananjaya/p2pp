from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from dotenv import load_dotenv
import os

db = SQLAlchemy()


def create_app():
    load_dotenv()
    app = Flask(__name__)

    app.config['SECRET_KEY'] = os.getenv("APP_SECRET_KEY")
    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("DATABASE_URI")

    db.init_app(app)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    from p2p.models.user import User
    from p2p.models.account import Account
    from p2p.models.transaction import Transaction

    @login_manager.user_loader
    def load_user(user_email):
        return User.query.get(str(user_email))

    from p2p.controllers import authentication, dashboard
    app.register_blueprint(authentication.userAuth)
    app.register_blueprint(dashboard.userDashboard)

    return app
