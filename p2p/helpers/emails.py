from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
import os


def sendCreditedEmail(to, credit_from, credit_amount):
    message = Mail(
        from_email=os.getenv("SENDGRID_AUTHORIZED_SENDER"),
        to_emails=to,
        subject="p2p Payments transaction information",
        html_content="""Your account credited Rs. {} by {}. Please follow bellow link to check your balance.
        <a href="{}">Link</a>
        """.format(credit_amount, credit_from, os.getenv("ENDPOINT")))
    try:
        sg = SendGridAPIClient(
            "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")
        response = sg.send(message)
    except Exception as e:
        print(e.message)
