# Dev Setup
- pip3 install virtualenv
- cd p2pP
- virtualenv venv
- source ./venv/bin/activate
- pip3 install -r requirements.txt
- export FLASK_APP=p2p
- flask run